package pl.pawelzadykowicz.pb01_05_resources;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity05 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_activity05);
		getWindow().setBackgroundDrawableResource(R.drawable.activity_background);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity05, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId)
		{
		case R.id.menu_start_second_activity:
			Intent intent = new Intent(MainActivity05.this, MainActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.asdf2, 0);
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}
		
		
	}

}
